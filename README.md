[![License: MIT](https://img.shields.io/badge/License-MIT-green.svg)](https://opensource.org/licenses/MIT)

# Kryonet_JCS

The [AS3][] socket library leveraging [Kryonet][].


<!-- Links -->

[AS3]: https://help.adobe.com/en_US/FlashPlatform/reference/actionscript/3/index.html
[Kryonet]: https://github.com/EsotericSoftware/kryonet
